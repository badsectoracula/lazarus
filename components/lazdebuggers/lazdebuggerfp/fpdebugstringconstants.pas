unit FpDebugStringConstants;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

resourcestring
  drsKeyForAddress = 'Key for Address';
  drsKeyForTypename = 'Key for Typename';

  drsFunctionName = 'Function name';
  drsCallSysVarToLStr = 'Call SysVarToLStr';
  drsCallJsonForDebug = 'Call JsonForDebug';

implementation

end.

